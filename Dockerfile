FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4
MAINTAINER Mehdi Kouhen <arantes555@gmail.com>

ARG SICKCHILL_VERSION="v2020.11.24-1"

RUN apt-get update -y
RUN apt-get install -y locales

# Locales so apps are happy
RUN locale-gen en_US.UTF-8
ENV LANG="en_US.UTF-8"
ENV LANGUAGE="en_US:en"
ENV LC_ALL="en_US.UTF-8"

## SickChill
# Setting the encoding for python, otherwise we get weird errors
ENV PYTHONIOENCODING="UTF-8"
RUN mkdir -p /app/code/sickchill
WORKDIR /app/code
RUN curl -L "https://github.com/SickChill/SickChill/archive/${SICKCHILL_VERSION}.tar.gz" \
    | tar -xz --strip-components=1 -C /app/code/sickchill
RUN ln -s /app/data/sickchill_cache /app/code/sickchill/sickchill/gui/slick/cache

COPY sickchill.ini /app/code/sickchill.ini

COPY start.sh /app/code/

EXPOSE 8081

CMD [ "/app/code/start.sh" ]
