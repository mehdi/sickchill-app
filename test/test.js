#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict'

require('chromedriver')

const execSync = require('child_process').execSync,
  expect = require('expect.js'),
  path = require('path')
const assert = require('assert')

const By = require('selenium-webdriver').By,
  until = require('selenium-webdriver').until,
  Key = require('selenium-webdriver').Key,
  Builder = require('selenium-webdriver').Builder

const username = process.env.TEST_USERNAME || process.env.USERNAME
const password = process.env.TEST_PASSWORD || process.env.PASSWORD

if (!username || !password) {
  console.log('USERNAME and PASSWORD env vars need to be set')
  process.exit(1)
}

describe('Application life cycle test', function () {
  this.timeout(0)

  let server
  const browser = new Builder().forBrowser('chrome').build()

  const LOCATION = 'test'
  const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' }
  const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000

  let app

  const getAppInfo = () => {
    const inspect = JSON.parse(execSync('cloudron inspect'))
    app = inspect.apps.filter(a => a.location === LOCATION || a.location === LOCATION + '2')[0]
    expect(app).to.be.an('object')
  }

  const login = () => browser.get(`https://${app.fqdn}/login`)
    .then(() => browser.wait(until.elementLocated(By.xpath('//input[@name="username"]')), TIMEOUT))
    .then(() => browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username))
    .then(() => browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password))
    .then(() => browser.findElement(By.tagName('form')).submit())
    .then(() => browser.wait(until.elementLocated(By.className('navbar-brand')), TIMEOUT))

  const addShowDirectory = () => browser.get(`https://${app.fqdn}/config/general/`)
    .then(() => browser.wait(until.elementLocated(By.id('addRootDir')), TIMEOUT))
    .then(() => browser.findElement(By.id('addRootDir')).click())
    .then(() => browser.findElement(By.css('.fileBrowserFieldContainer > input:nth-child(1)')).sendKeys('/app/data/TVShows'))
    .then(() => browser.findElement(By.css('button.ui-button:nth-child(1)')).click())
    .then(() => browser.findElement(By.css('#rootDirs option[value*="TVShows"]')))

  const addShow = () => browser.get(`https://${app.fqdn}/addShows/newShow/`)
    .then(() => browser.wait(until.elementLocated(By.id('show-name')), TIMEOUT))
    .then(() => browser.findElement(By.id('show-name')).sendKeys('Friends'))
    .then(() => browser.findElement(By.id('search-button')).click())
    .then(() => browser.wait(until.elementLocated(By.css('input[value*="id=|79168|Friends|1994-9-22|false"]')), TIMEOUT))
    .then(() => browser.findElement(By.css('input[value*="id=|79168|Friends|1994-9-22|false"]')).click())
    .then(() => browser.findElement(By.id('addShowButton')).click())
    .then(() => browser.getCurrentUrl())
    .then(url => assert.strictEqual(url, `https://${app.fqdn}/home/`))

  const isShowThere = () => browser.get(`https://${app.fqdn}/home/`)
    .then(() => browser.findElement(By.css('.show-container[data-name*=friends]')))

  const logout = () => browser.get(`https://${app.fqdn}/logout`)
    .then(() => browser.sleep(2000)) // wait for "connection lost" error
    .then(() => browser.get(`https://${app.fqdn}/login`))
    .then(() => browser.wait(until.elementLocated(By.xpath('//input[@name="username"]')), TIMEOUT))

  before(() => {
    if (!password) throw new Error('PASSWORD env var not set')

    const seleniumJar = require('selenium-server-standalone-jar')
    const SeleniumServer = require('selenium-webdriver/remote').SeleniumServer
    server = new SeleniumServer(seleniumJar.path, { port: 4444 })
    server.start()
  })

  after(() => {
    browser.quit()
    server.stop()
  })

  xit('build app', () => { execSync('cloudron build', EXEC_ARGS) })

  it('install app', () => { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS) })

  it('can get app information', getAppInfo)

  it('can login', login)
  it('add shows directory', addShowDirectory)
  it('add show', addShow)
  it('is show there', isShowThere)
  it('can logout', logout)

  it('can restart app', () => { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS) })
  it('can login', login)
  it('is show there', isShowThere)
  it('can logout', logout)

  it('backup app', () => { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS) })
  it('restore app', () => {
    const backups = JSON.parse(execSync('cloudron backup list --raw'))
    execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS)
    execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS)
    const inspect = JSON.parse(execSync('cloudron inspect'))
    app = inspect.apps.filter(a => a.location === LOCATION)[0]
    execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS)
  })

  it('can login', login)
  it('is show there', isShowThere)
  it('can logout', logout)

  it('move to different location', () => { execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS) })
  it('can get app information', getAppInfo)
  it('can login', login)
  it('is show there', isShowThere)
  it('can logout', logout)

  it('uninstall app', () => { execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS) })

  // test update
  it('can install app', () => { execSync(`cloudron install --appstore-id io.cloudron.sickchill --location ${LOCATION}`, EXEC_ARGS) })
  it('can get app information', getAppInfo)
  it('can update', () => { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS) })
  it('can login', login)
  it('is show there', isShowThere)
  it('can logout', logout)
  it('uninstall app', () => { execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS) })
})
