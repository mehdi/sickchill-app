#!/bin/bash

set -eu

# Ensure Sickchill settings exist
if [ ! -f /app/data/sickchill.ini ]; then
  cp /app/code/sickchill.ini /app/data/sickchill.ini
fi

# Setting up SickChill data dir
if [ ! -d /app/data/sickchill ]; then
  mkdir -p /app/data/sickchill
fi

# Setting up SickChill UI cache
mkdir -p /app/data/sickchill_cache

mkdir -p /app/data/TVShows

chown -R cloudron:cloudron /app/data /tmp /run

echo "Starting SickChill..."
/usr/local/bin/gosu cloudron:cloudron python3 /app/code/sickchill/SickChill.py --nolaunch --datadir=/app/data/sickchill --config=/app/data/sickchill.ini
