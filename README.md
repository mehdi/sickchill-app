# SickChill

Automatic Video Library Manager for TV Shows.

## Building

### Cloudron
The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
git clone https://git.cloudron.io/mehdi/sickchill-app.git
cd sickchill-app
cloudron build
cloudron install
```
